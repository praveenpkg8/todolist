import os
from flask import Flask, request, jsonify
from models import db, Notes

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'data.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db.init_app(app)



@app.route("/", methods = ["GET", "POST"])
def welcome():
    value = request.json['created_by']
    return "welcome to todo app " + value

@app.route("/note/save", methods = ["GET","POST"])
def save():
    created_by = request.json['created_by']
    content = request.json['content']
    final_content = Notes(created_by, content)
    db.session.add(final_content)
    db.session.commit()
    return jsonify({'content': final_content.content, "created_by": final_content.created_by})

@app.route("/notes", methods = ["GET"])
def view():
    active_notes = Notes.query.filter_by(is_active = True).all()
    data = list()
    for note in active_notes:
        final_content = {
            'id': note.id,
            'content': note.content,
            'created_by': note.created_by,
            'created_on': note.created_on,
            'is_active': note.is_active
        }
        data.append(final_content)
    return jsonify({'data': data})

@app.route("/note/<int:note_id>", methods = ['GET'])
def view_one(note_id):
    note = Notes.view_by_id(note_id)
    if note is None:
        return jsonify({"error": "No id found"})
    return jsonify({'content': note.content, 'created_by': note.created_by, 'created_on': note.created_on, 'is_active': note.is_active})


@app.route("/note/update/<int:note_id>", methods=['PUT'])
def update(note_id):
    note = Notes.view_by_id(note_id)
    note.content = request.json['content']
    db.session.commit()
    return jsonify({'content': note.content, 'created_by': note.created_by, 'created_on': note.created_on, 'is_active': note.is_active})

@app.route("/note/delete/<int:note_id>", methods=['DELETE'])
def delete(note_id):
    note = Notes.view_by_id(note_id)
    note.is_active = False
    db.session.commit()
    return jsonify({'content': note.content, 'created_by': note.created_by})


if __name__ == '__main__':
    app.run(debug= True)